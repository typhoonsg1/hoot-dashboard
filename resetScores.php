<?php

require_once('setup.php');

$conn = new mysqli($servername, $username, $password, $database);

if($conn->connect_error){
  die("connection failed" . $conn->connect_error);
}

$sql = "TRUNCATE TABLE teamScores";

if($conn->query($sql) === TRUE){
  echo "table cleared";
}
else{
  echo "No table cleared";
}

?>
