<?php

require_once('setup.php');

$sql = "CREATE DATABASE $database";

$conn = new mysqli($servername, $username, $password);
//check connection
if($conn->connect_error){
  die("connection failed" . $conn->connect_error);
}

if($conn->query($sql) === TRUE){
  echo "DB Created";
}
else{
  echo "No DB Created";
}

$conn = new mysqli($servername, $username, $password, $database);

if($conn->connect_error){
  die("connection failed" . $conn->connect_error);
}

$sql = "CREATE TABLE teamScores(
  teamName VARCHAR(30) NOT NULL UNIQUE,
  score VARCHAR(300) NOT NULL
)";

if($conn->query($sql) === TRUE){
  echo "table Created";
}
else{
  echo "No table Created";
}

$conn->close();
 ?>
