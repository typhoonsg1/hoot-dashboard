<?php

require_once('setup.php');

echo "<table class='table table-striped'>";

$sql = "SELECT * FROM teamScores";

$conn = new mysqli($servername, $username, $password, $database);

$result = $conn->query($sql);

if($result->num_rows > 0){
  while($row = $result->fetch_assoc()){
    echo "<tr>
      <td width='20%'>
        <h3 style='padding:0;margin-top:5px;'>".$row['teamName']."</h3>
      </td>
      <td>
        <div class='progress active'>
          <div class='progress-bar progress-bar-custom' role='progressbar' aria-valuenow='70' aria-valuemin='0' aria-valuemax='100' style='width:".$row['score']."%;'>

          </div>
        </div>
      </td>
    </tr>";
  }
}


echo "</table>";
